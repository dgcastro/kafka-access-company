package br.com.accesscontrol.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccesscontrolProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccesscontrolProducerApplication.class, args);
	}

}
