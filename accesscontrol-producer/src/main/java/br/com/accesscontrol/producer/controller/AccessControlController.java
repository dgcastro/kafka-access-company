package br.com.accesscontrol.producer.controller;


import br.com.accesscontrol.producer.model.AccessRequest;
import br.com.accesscontrol.producer.service.AccessControlProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccessControlController {

    @Autowired
    private AccessControlProducer accessControlProducer;

    @PostMapping("/accesscontrol/{customerId}/{doorId}")
    public boolean accessControl(@PathVariable long customerId, @PathVariable long doorId){
        return accessControlProducer.sendToKafka(new AccessRequest(customerId,doorId));
    }
}
