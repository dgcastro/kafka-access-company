package br.com.accesscontrol.producer.service;

import br.com.accesscontrol.producer.model.AccessRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AccessControlProducer {
    @Autowired
    private KafkaTemplate<String, AccessRequest> producer;

    public boolean sendToKafka(AccessRequest accessRequest){

        if(new Random().nextInt(100) %2==0) {
            producer.send("spec3-danilo-gouveia-2", accessRequest);
            return  true;
        }else{
            return false;
        }
    }
}
