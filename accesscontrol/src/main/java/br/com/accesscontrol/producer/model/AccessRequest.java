package br.com.accesscontrol.producer.model;


public class AccessRequest {
    private long customerId;
    private long doorId;

    public AccessRequest(){

    }

    public AccessRequest(long customerId, long doorId) {
        this.customerId = customerId;
        this.doorId = doorId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public long getDoorId() {
        return doorId;
    }

    public void setDoorId(long doorId) {
        this.doorId = doorId;
    }
}
