package br.com.accesscontrol;

import br.com.accesscontrol.producer.model.AccessRequest;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;


@Component
public class AccessControl {
    @KafkaListener(topics = "spec3-danilo-gouveia-2", groupId = "AccessControl")

    public void consumer(@Payload AccessRequest accessRequest){
        String CSV = "/home/access.csv";
        try{
            CSVWriter writer = new CSVWriter(new FileWriter(CSV, true));

            String record = String.valueOf(accessRequest.getCustomerId())
                        +","
                        +String.valueOf(accessRequest.getDoorId())
                        +","
                        + LocalDateTime.now().toString();
            writer.writeNext(record);
            writer.close();
        }catch (IOException exception){
            System.out.println("ERRO!");
        }
    }
}
